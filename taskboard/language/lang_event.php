<?php
/* @authorcode  codestrings
 * @copyright   Leyun internet Technology(Shanghai)Co.,Ltd
 * @license     http://www.dzzoffice.com/licenses/license.txt
 * @package     DzzOffice
 * @link        http://www.dzzoffice.com
 * @author      zyx(zyx@dzz.cc)
 */
if(!defined('IN_DZZ')) {
	exit('Access Denied');
}
$eventshow=array(
	//任务版
	'taskboard_create'	=>array('icon'=>'dzz dzz-view-list',			'color'=>'primary'),
	'taskboard_archive'	=>array('icon'=>'dzz dzz-archive',	          	'color'=>'purplee'),
	'taskboard_active'	=>array('icon'=>'dzz dzz-view-list-alt',			'color'=>'yellow'),
	'taskboard_restore'	=>array('icon'=>'dzz dzz-view-list-alt',			'color'=>'yellow'),
	'taskboard_delete'	=>array('icon'=>'dzz dzz-view-list-alt',			'color'=>'danger'),

    //任务列表
	'task_cat_create'	=>array('icon'=>'dzz dzz-view-list',			'color'=>'primary'),
	'task_cat_delete'	=>array('icon'=>'dzz dzz-view-list',			'color'=>'danger'),
	'task_cat_rename'	=>array('icon'=>'dzz dzz-view-list',			'color'=>'warning'),
	'task_cat_restore'	=>array('icon'=>'dzz dzz-view-list',			'color'=>'yellow'),
	'task_cat_active'	=>array('icon'=>'dzz dzz-view-list',			'color'=>'yellow'),
	'task_cat_archive'	=>array('icon'=>'dzz dzz-archive',	'color'=>'purplee'),
	
	//任务
	'task_create'		=>array('icon'=>'dzz dzz-add',			'color'=>'primary'),
	'task_restore'		=>array('icon'=>'dzz dzz-restore',		'color'=>'yellow'),
	'task_active'		=>array('icon'=>'dzz dzz-restore',		'color'=>'yellow'),
	'task_archive'		=>array('icon'=>'dzz dzz-archive',	'color'=>'warning'),
	'task_completed'	=>array('icon'=>'dzz dzz-check-box',		'color'=>'success'),
	'task_uncompleted'	=>array('icon'=>'dzz dzz-check-box-outline-blank',	'color'=>'danger'),
	'task_delete'		=>array('icon'=>'dzz dzz-delete',		'color'=>'danger'),
	'task_move'			=>array('icon'=>'dzz dzz-arrow-forward',	'color'=>'warning'),
	
	//任务标签
	'task_label_add'	=>array('icon'=>'dzz dzz-label',		'color'=>'primary'),
	'task_label_remove'=>array('icon'=>'dzz dzz-label',		'color'=>'danger'),
	
	//任务关注
	'task_follow_add'	=>array('icon'=>'dzz dzz-visibility',		'color'=>'purplee'),
	'task_follow_remove'=>array('icon'=>'dzz dzz-visibility-off',	'color'=>'danger'),
	
	//任务分配
	'task_assign_add'	=>array('icon'=>'dzz dzz-member',			'color'=>'success'),
	'task_assign_remove'=>array('icon'=>'dzz dzz-member',			'color'=>'danger'),
	
	//任务截止时间
	'task_endtime_add'		=>array('icon'=>'dzz dzz-clock',		'color'=>'yellow'),
	'task_endtime_change'	=>array('icon'=>'dzz dzz-clock',		'color'=>'yellow'),
	'task_endtime_cancel'	=>array('icon'=>'dzz dzz-clock',		'color'=>'yellow'),
	
	//任务附件
	'task_attach_add'		=>array('icon'=>'dzz dzz-attachment','color'=>'primary'),
	'task_attach_delete'	=>array('icon'=>'dzz dzz-attachment','color'=>'danger'),
	'task_attach_restore'	=>array('icon'=>'dzz dzz-attachment','color'=>'primary'),
	
	
	//任务检查项
	'task_sub_complete'		=>array('icon'=>'dzz dzz-check-box',	'color'=>'success'),
	'task_sub_uncomplete'	=>array('icon'=>'dzz dzz-check-box-outline-blank','color'=>'danger'),
	'task_sub_add'			=>array('icon'=>'dzz dzz-menu',	'color'=>'primary'),
	'task_sub_delete'		=>array('icon'=>'dzz dzz-menu',	'color'=>'danger'),
	'task_sub_rename'		=>array('icon'=>'dzz dzz-menu',	'color'=>'yellow'),
	
	
	//任务工时
	'task_worktime_add'		=>array('icon'=>'dzz dzz-clock','color'=>'primary'),
	'task_worktime_cancel'	=>array('icon'=>'dzz dzz-clock','color'=>'danger'),
	'task_worktime_change'	=>array('icon'=>'dzz dzz-clock','color'=>'purplee'),
	
	//任务预算
	'task_money_add'		=>array('icon'=>'dzz dzz-money','color'=>'primary'),
	'task_money_cancel'		=>array('icon'=>'dzz dzz-money','color'=>'danger'),
	'task_money_change'		=>array('icon'=>'dzz dzz-money','color'=>'purplee'),
	
	'task_comment_add'		=>array('icon'=>'dzz dzz-comment','color'=>'primary'),
	'task_comment_delete'	=>array('icon'=>'dzz dzz-comment','color'=>'danger'),
	'task_comment_at'		=>array('icon'=>'dzz dzz-comment','color'=>'purplee'),
	);