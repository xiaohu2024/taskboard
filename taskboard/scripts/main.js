/* @authorcode  codestrings
 * @copyright   Leyun internet Technology(Shanghai)Co.,Ltd
 * @license     http://www.dzzoffice.com/licenses/license.txt
 * @package     DzzOffice
 * @link        http://www.dzzoffice.com
 * @author      zyx(zyx@dzz.cc)
 */
function setSave(h,t,i){if("title"==h&&""==t)return showmessage("名称不能为空","danger",1e3,1),void jQuery("#title_1").focus();jQuery.post(ajaxurl+"&do=setSave&orgid="+i,{name:h,val:t})}function setImage(t,i,e,s){"100%"==i&&(i=jQuery(t).parent().width()),"100%"==e&&(e=jQuery(t).parent().height()),imgReady(t.src,function(){width=this.width,height=this.height,i/e>width/height?(w=s?i:width>i?i:width,h=w*(height/width)):(h=s?e:height>e?e:height,w=h*(width/height)),width<=i&&height<=e?(s||(w=width,h=height),jQuery(t).css("margin-top",(e-h)/2).css("margin-left",(i-w)/2).css("width",w).css("height",h)):height<e&&width>i?(s||(w=i,h=w*(height/width)),jQuery(t).css("margin-top",(e-h)/2).css("margin-left",(i-w)/2).css("width",w).css("height",h)):height>e&&width<i?(s||(h=clientHidth,w=h*(width/height)),jQuery(t).css("margin-top",(e-h)/2).css("margin-left",(i-w)/2).css("width",w).css("height",h)):jQuery(t).css("margin-top",(e-h)/2).css("margin-left",(i-w)/2).css("width",w).css("height",h)})}